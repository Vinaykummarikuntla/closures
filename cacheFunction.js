exportObjects = {
    cacheFunction (cb) {
        let cache = {};
        return function (...parameters) {
            if (parameters in cache) {
                return cache[parameters];
            }
            else {
                cache[parameters] = cb(parameters);
                return cache[parameters]
            }
        }
    }
};
module.exports = {exportObjects};