
exportObjects = {
    counterFactory () {
        let _counter = 0;
        return {
            increment () {
                _counter += 1;
                return _counter;
            },
            decrement () {
                _counter -= 1;
                return _counter;
            }
        };
    }
};
module.exports = {exportObjects};